from starlette.applications import Starlette
from starlette.templating import Jinja2Templates
from starlette.routing import Router, Mount
from starlette.staticfiles import StaticFiles
from starlette.config import Config
import uvicorn

templates = Jinja2Templates(directory='web/templates')

app = Starlette(routes=[
    Mount('/static', app=StaticFiles(directory='web/static'), name="static"),
])

config = Config("env")
app.debug = config('DEBUG', cast=bool, default=False)
web_url=config('WEB_URL', cast=str, default="0.0.0.0")
web_port=config('WEB_PORT', cast=int, default=5005)
api_url=config('API_URL', cast=str, default="0.0.0.0")
api_port=config('API_PORT', default=5010)
app.api_base="https://intercambiosoceanicos.iib.unam.mx/embedding_api"
#app.api_base="http://132.247.131.220:5005"

g_min_sim=0.4
g_min_sim_adaptive=0.60
g_max_adaptive_terms=20
g_step=1
g_win_size_prev=0
g_win_size_next=2
g_max_links=400
g_only_seed="off"
g_word_boost=1.5
g_top_related=100
g_smoothing="linear"
g_adaptive="off"


def param_arg(request,key,def_val):
      try:
          return request.query_params[key]
      except KeyError:
          return def_val

def get_params(request):
    pars={}
    pars['min_sim'] = float(param_arg(request,'min_sim',g_min_sim))
    pars['step'] = int(param_arg(request,'step',g_step))
    pars['win_size'] = int(param_arg(request,'win_size',4))
    pars['win_size_prev'] = int(param_arg(request,'win_size_prev',g_win_size_prev))
    pars['win_size_next'] = int(param_arg(request,'win_size_next',g_win_size_next))
    pars['max_links'] = int(param_arg(request,'max_links',g_max_links))
    pars['top_related'] = int(param_arg(request,'top_related',g_top_related))
    pars['word_boost'] = float(param_arg(request,'word_boost',g_word_boost))
    pars['smoothing'] = param_arg(request,'smoothing',g_smoothing)
    pars['only_seed'] = bool(param_arg(request,'only_seed',g_only_seed)=='on')
    pars['adaptive'] = bool(param_arg(request,'adaptive',g_adaptive)=='on')
    pars['min_sim_adaptive'] = float(param_arg(request,'min_sim_adaptive',g_min_sim_adaptive))
    pars['max_adaptive_terms'] = int(param_arg(request,'max_adaptive_terms',g_max_adaptive_terms))
    return pars

@app.route('/')
async def years(request):
    return templates.TemplateResponse('index.html', {'request': request})

@app.route('/random_embeddings/{year}')
async def random_embeddings(request):
    year = int(request.path_params['year'])
    return templates.TemplateResponse('random_embeddings.html', {'request': request,'year':year, 'api_url': app.api_base})


@app.route('/embeddings/{year}')
async def embeddings(request):
    year = int(request.path_params['year'])
    try:
        terms = [t for t in [ t.strip() for t in request.query_params['q'].split(' ')] if len(t) > 0]
        return templates.TemplateResponse('embeddings.html', {'request': request,'year':year,'terms':terms, 'api_url': app.api_base})
    except KeyError:
        return templates.TemplateResponse('embeddings.html', {'request': request,'year':year,'api_url': app.api_base})

@app.route('/network/{year}')
async def network(request):
    year = int(request.path_params['year'])

    year_ = param_arg(request,'year',None)
    year = year_ if year_ else year

    try:
        terms = [t for t in [ t.strip() for t in request.query_params['q'].split(' ')] if len(t) > 0]
        return templates.TemplateResponse('network.html', {'request': request,'year':year,'terms':terms,'api_url': app.api_base})
    except KeyError:
        return templates.TemplateResponse('network.html', {'request': request,'year':year,'api_url': app.api_base})

@app.route('/networks/{ini_year}/{fin_year}')
async def embeddings(request):
    ini_year = int(request.path_params['ini_year'])
    fin_year = int(request.path_params['fin_year'])

    ini_year_ = param_arg(request,'ini_year',None)
    fin_year_ = param_arg(request,'fin_year',None)
    ini_year  = int(ini_year_) if ini_year_ else ini_year
    fin_year  = int(fin_year_) if fin_year_ else fin_year

    parameters=get_params(request)

    args=request.url.query
    try:
        terms = [t for t in [ t.strip() for t in request.query_params['q'].split(' ')] if len(t) > 0]
        parameters.update({'request': request,
                    'ini_year':ini_year,
                    'fin_year':fin_year,
                    'terms':terms,
                    'args':args,
                    'url':'networks','api_url': app.api_base})
        return templates.TemplateResponse('networks.html', parameters)
    except KeyError:
        parameters.update({'request': request,
                    'ini_year':ini_year,
                    'fin_year':fin_year,
                    'args':args,
                    'url':'networks','api_url': app.api_base})
        return templates.TemplateResponse('networks.html', parameters)

@app.route('/networks_aggregate/{ini_year}/{fin_year}')
async def embeddings(request):
    ini_year = int(request.path_params['ini_year'])
    fin_year = int(request.path_params['fin_year'])

    ini_year_ = param_arg(request,'ini_year',None)
    fin_year_ = param_arg(request,'fin_year',None)
    ini_year  = int(ini_year_) if ini_year_ else ini_year
    fin_year  = int(fin_year_) if fin_year_ else fin_year

    parameters=get_params(request)

    args=request.url.query

    try:
        terms = [t for t in [ t.strip() for t in request.query_params['q'].split(' ')] if len(t) > 0]
        parameters.update({'request': request,
                    'ini_year':ini_year,
                    'fin_year':fin_year,
                    'terms':terms,
                    'args':args,
                    'url':'networks_aggregate','api_url': app.api_base})
        return templates.TemplateResponse('networks.html',parameters)
    except KeyError:
        parameters.update({'request': request,
                    'ini_year':ini_year,
                    'fin_year':fin_year,
                    'terms':terms,
                    'args':args,
                    'url':'networks_aggregate','api_url': app.pi_base}.update(parameters))
        return templates.TemplateResponse('networks.html',parameters)

@app.route('/stream/{ini_year}/{fin_year}')
async def stream(request):
    ini_year = int(request.path_params['ini_year'])
    fin_year = int(request.path_params['fin_year'])

    ini_year_ = param_arg(request,'ini_year',None)
    fin_year_ = param_arg(request,'fin_year',None)
    ini_year  = int(ini_year_) if ini_year_ else ini_year
    fin_year  = int(fin_year_) if fin_year_ else fin_year

    parameters=get_params(request)

    args=request.url.query
    
    try:
        terms = [t for t in [ t.strip() for t in request.query_params['q'].split(' ')] if len(t) > 0]
        parameters.update({'request': request,
                    'ini_year':ini_year,
                    'fin_year':fin_year,
                    'terms':terms,
                    'args':args,
                    'url':'networks_aggregate','api_url': app.api_base})
        return templates.TemplateResponse('stream.html',parameters)
    except KeyError:
        parameters.update({'request': request,
                    'ini_year':ini_year,
                    'fin_year':fin_year,
                    'args':args,
                    'url':'networks_aggregate','api_url': app.api_base}.update(parameters))
        return templates.TemplateResponse('stream.html',parameters)

if __name__ == '__main__':
    uvicorn.run(app, host=web_url, port=web_port)
