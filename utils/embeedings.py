import os
import os.path
import click
import subprocess
from colorama import Fore, Style, Back
from pymagnitude import Magnitude
from collections import Counter
from gensim.models import KeyedVectors

def extract_years(filename):
    name,ext=os.path.splitext(filename)
    res=name.split("_")
    return int(res[0]),int(res[1]),int((int(res[0])+int(res[1]))/2),filename

@click.group()
def cli():
    pass


@cli.command()
@click.option("--ini_year",default=1821,
                help='Initial year to analyze')
@click.option("--fin_year",default=1910,
                help='Fin year to analyze')
@click.option("--model_dir",default="models",
                help='Directory with models')
def stats(model_dir,ini_year,fin_year):
    models=[extract_years(x) for x in os.listdir(model_dir) if x.endswith("magnitude")]
    models.sort()
    lengths=[]
    for _,_,year,filename in models:
        if year < ini_year or year > fin_year:
            continue
        print(Fore.BLUE+Style.BRIGHT,"Loading",filename, Style.NORMAL)
        model_=Magnitude(os.path.join(model_dir,filename))

        lengths.append(len(model_))
    print(Fore.YELLOW+Style.BRIGHT,"Stats", Style.NORMAL)
    print(Fore.YELLOW,"{:20}: {}".format("Average vocabulary",sum(lengths)/len(lengths)), Style.NORMAL)
    print(Fore.YELLOW,"{:20}: {}".format("Min vocabulary",min(lengths)), Style.NORMAL)
    print(Fore.YELLOW,"{:20}: {}".format("Max vocabulary",max(lengths)), Style.NORMAL)


@cli.command()
@click.option("--ini_year",default=1821,
                help='Initial year to analyze')
@click.option("--fin_year",default=1910,
                help='Fin year to analyze')
@click.option("--model_dir",default="models",
                help='Directory with models')
def convert(model_dir,ini_year,fin_year):
    models=[extract_years(x) for x in os.listdir(model_dir) if x.endswith("w2v") or x.endswith("bin")]
    models.sort()
    for _,_,year,filename in models:
        if year < ini_year or year > fin_year:
            continue
        if filename.endswith("w2v"):
            print(Fore.BLUE+Style.BRIGHT,"Opening",filename, Style.NORMAL)
            model_=KeyedVectors.load(os.path.join(model_dir,filename))
            print(Fore.BLUE+Style.BRIGHT,"Saving it as binary",filename, Style.NORMAL)
            model_.save_word2vec_format(os.path.join(model_dir,filename.replace(".w2v",".bin")),binary=True)
            filename=filename.replace(".w2v",".bin")
        cmd = ["python","-m","pymagnitude.converter","-i"]
        cmd.append(os.path.join(model_dir,filename))
        cmd.append("-o")
        cmd.append(os.path.join(model_dir,filename.replace(".bin",".magnitude")))
        print(Fore.BLUE+Style.BRIGHT,"Convertion cmd"," ".join(cmd), Style.NORMAL)
        subprocess.run(cmd)






if __name__ == '__main__':
    cli()
