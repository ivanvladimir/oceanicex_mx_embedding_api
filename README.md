
API for embeddings through time. Inspired by [Shifting Concepts Through Time project](https://github.com/NLeSC/ShiCo).

Part of the [Oceanic Exchanges project](https://oceanicexchanges.org) by the [Mexican team](https://oceanicexchanges.org/mx).

# Description

This is the API for quering a collection of embeddings through time. This contains:

* api: the code for the api
* web: some code that exemplifies the api and the visualization posibilities of it
* doc: documentation of the api, using [apidoc](https://pypi.org/project/ApiDoc/)
* utils: to convert the w2v files to [magnitue](https://github.com/plasticityai/magnitude)

# Installation

Requirements:

* python-dev python-setuptools


First, you have to clone the code

```
git clone https://gitlab.com/ivanvladimir/oceanicex_mx_embedding_api.git
```

Create an environment, activate and `pip install` the requirements.

```
python36 -m venv environment
source environment/bin/activate
pip3 install -r requirements.txt
```

# Converting w2v models to magnitude

The API needs to read the models in a magnitude format, to convert your w2v models use:

```
python36 utils/embeedings.py convert --model_dir models/
```

# Launching API

First you have to configure the service in the `env` file. The API can be launch with `uvicorn`, adapt with the port number corresponding used in `env` file.


```
uvicorn api.app:app --port 5010
```

Open your browser and go to the url defines in `dev` with the specified `url`, and you could ask some queries to the API.

# Launching _web_: demo

The _web_ directory contains some examples that visualize the results from the API `uvicorn`, adapt with the port number corresponding used in `env` file.

```
uvicorn wev.app:app --port 5005
```

Open your browser and go to the url defines in `dev` with the specified `url`, and you will see some visualization examples.

# Visualize API documentation

Go to the documentation directory `doc/output'.

```
python3 -m http.server
```
Open your browser and go to the printed url and you will be able to see some documentation of the API.


