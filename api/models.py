import os
import os.path
from pymagnitude import Magnitude
from collections import defaultdict, Counter
import numpy as np

def extract_years(filename):
    name,ext=os.path.splitext(filename)
    res=name.split("_")
    return int((int(res[0])+int(res[1]))/2),filename

def load_models(model_dir):
    models=[extract_years(x) for x in os.listdir(model_dir) if x.endswith("magnitude")]
    models.sort()
    models_={}
    for year,filename in models:
        model_=Magnitude(os.path.join(model_dir,filename))
        models_[year]=model_
    return models_

def get_stream(network_,terms_,ini_year,fin_year, step=1,only_seed=True, word_boost=1.5):
    terms=set()
    stream={}
    for i,year_ in enumerate(range(ini_year,fin_year+1,step)):
        nodes=network_[i]['nodes']
        for n in nodes:
            terms.add(n['label'])
    for t in terms:
        stream[t]={}

    for i,year_ in enumerate(range(ini_year,fin_year+1,step)):
        nodes=network_[i]['nodes']
        links=network_[i]['edges']
        i2l=dict([(n['id'],n['label']) for n in nodes])
        for n in nodes:
            if n['group']==0:
                stream[n['label']][year_]=word_boost
            else:
                stream[n['label']][year_]=0.0

        for l in links:
            stream[i2l[l['to']]][year_]+=l['value']

    stream_=[]
    labels=list(range(ini_year,fin_year+1,step))
    final_terms=terms_ if only_seed else terms

    for term in final_terms:
        info=stream[term]
        values=[ info[y] if y in info else 0.0 for y in range(ini_year,fin_year+1,step)]
        stream_.append({'dataset_label':term, 'dataset_values':values})
    
    return (labels,stream_)

def extract_network(vecs,terms,new_terms,top_related=10, min_sim=0.1,adaptive=False,min_sim_adaptive=0.85,max_adaptive_terms=10):
    words={}
    n_terms=set(terms)
    new_terms_={}
    if adaptive:
        n_terms=n_terms.union(new_terms)
    n_related=set()
    links={}
    for term in n_terms:
        if not term in vecs:
            continue
        for k,v in vecs.most_similar_cosmul(term, topn = top_related):
            if v < min_sim:
                continue
            if adaptive:
                if v > min_sim_adaptive:
                    try:
                        new_terms_[k]=max(v,new_terms_[k])
                    except KeyError:
                        new_terms_[k]=v
            try:
                links[(term,k)]+=v
            except:
                links[(term,k)]=v
            if not k in n_terms:
                n_related.add(k)

    new_terms=set([k for k,v in Counter(new_terms_).most_common(max_adaptive_terms)])

    nodes=list(n_terms.union(n_related))
    nodes.sort()
    word2id=dict([(n,i) for i,n in enumerate(nodes)])
    nodes=[]
    edges=[]
    for i,n in enumerate(n_terms):
        if n in new_terms:
            nodes.append({'id':word2id[n],'label':n, 'group':4, 'size':'10'})
        else:
            nodes.append({'id':word2id[n],'label':n, 'group':0, 'size':"20"})

    for i,n in enumerate(n_related):
        if n in new_terms:
            nodes.append({'id':word2id[n],'label':n, 'group':2, 'size':"10"})
        else:
            nodes.append({'id':word2id[n],'label':n, 'group':1, 'size':"5"})
    for (o,d),v in links.items():
        edges.append({"from":word2id[o],'to':word2id[d],'value':float(v), 'title': "{:0.3}".format(v)})

    return nodes, edges, new_terms


def aggregate_networks(networks_,terms_,new_terms_,ini_year,fin_year,step=1,
        win_size_prev=0,win_size_next=0,max_links=1000,smoothing="linear",c_gauss=1.0):
    networks=[]
    node2label={}
    size=2*max(win_size_next,win_size_prev)+1
    weights=[1.0 for x in range(-win_size_prev,win_size_next+1,step)]

    if smoothing=="linear":
        weights=[1.0 for x in range(-win_size_prev,win_size_next+1,step)]
    elif smoothing=="gauss":
        space=np.linspace(-1.0,1.0,size)
        smoothing=(np.exp(-(space ** 2) / c_gauss))
        weights=[smoothing[x] for x in range(int(-win_size_prev+size/2-0.1),int(win_size_next+1+size/2-0.1),step)]
    for i,year in enumerate(range(ini_year,fin_year+1,step)):
        ini=max(0,i-win_size_prev)
        fin=max(len(networks_),i+win_size_next)
        chunk=networks_[ini:fin+1]
        links={}
        terms=set()
        words=set()
        groups={}
        l2g={}
        for j,year_ in enumerate(range(max(ini_year,year-win_size_prev),min(fin_year,year+win_size_next)+1,step)):
            if not year_ in node2label:
                try:
                    node2label[year_]=dict([(n['id'],n['label']) for n in chunk[j][0] ] )
                except IndexError:
                    pass
            l2g.update(dict([(n['label'],n['group']) for n in chunk[j][0] ] ))

            n2l=node2label[year_]
            c_nodes,c_links=chunk[j]
            for node in c_nodes:
                if node['group'] in [0,4]:
                    words.add(n2l[node['id']])
                    terms.add(n2l[node['id']])
                    if not n2l[node['id']] in links:
                        links[n2l[node['id']]]={}
            for link in c_links:
                try:
                    links[n2l[link['from']]][n2l[link['to']]]+=link['value']*weights[j]
                except KeyError:
                    links[n2l[link['from']]][n2l[link['to']]]=link['value']*weights[j]
                words.add(n2l[link['to']])
        
        nodes_=[]
        links_=[]
        words_used=set()
        word2id=dict([(n,i) for i,n in enumerate(words)])
        id2word=dict([(i,n) for n,i in word2id.items()])
        for f,f_links in links.items():
            if not f in words_used: 
                nodes_.append({"id":word2id[f],'label':f, 'group':l2g[f]})
                words_used.add(f)
        for f,f_links in links.items():
            for t,v in f_links.items():
                links_.append((word2id[f],word2id[t],v))
        links_.sort(key=lambda t: t[2],reverse=True)
        links_=[{'from':l[0],'to':l[1],'value':l[2],'title':"{:0.3}".format(l[2])} for l in links_[:max_links]]
        for link in links_:
            if not id2word[link['to']] in words_used:
                idnode=link['to']
                label=id2word[idnode]
                group=l2g[label]
                nodes_.append({"id":idnode,'label':label, 'group':group, 'size': 10 if group==1 else 15})
                words_used.add(id2word[link['to']])


        networks.append({
            'nodes':nodes_,
            'edges':links_,
            'title':str(year),
            'terms':list(terms),
            'total_terms': len(terms),
            'total_nodes': len(nodes_),
            'total_edges': len(links_),
 
            })
    return networks

