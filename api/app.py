from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.exceptions import HTTPException
from starlette.routing import Route, Router
from starlette.middleware.gzip import GZipMiddleware
from starlette.middleware.cors import CORSMiddleware
from starlette.config import Config
import uvicorn
import random
from api.models import *

# Loading models
models_ = load_models("models/")

app = Starlette(debug=True)
app.add_middleware(GZipMiddleware, minimum_size=1000)
app.add_middleware(CORSMiddleware, allow_origins=['*'])
config = Config("env")
app.debug = config('DEBUG', cast=bool, default=False)
api_url=config('API_URL', cast=str, default="0.0.0.0")
api_port=config('API_PORT', cast=int, default=5010)
app.api_base="http://{}:{}".format(api_url,api_port)


@app.exception_handler(4001)
async def no_models(request, exc):
   return JSONResponse({
       "error":"No models available for analysis",
        "description":" No model per year that allow the analysis",
        "error_code":"4001"},status_code=500)

@app.exception_handler(4002)
async def no_number(request, exc):
    return JSONResponse({
        'error':"Expected numeric year",
        "description":"The year must be a numeric value such as 1900 but it was no found in {}".format(exc.detail),
        "error_code":"4002"},status_code=500)

@app.exception_handler(4003)
async def no_year(request, exc):
    return JSONResponse({
        'error':"No year available in collection",
        "description":"No model for the year {}".format(exc.detail),
        "error_code":"4003"},status_code=500)

@app.exception_handler(4004)
async def no_good_parameter(request, exc):
    return JSONResponse({
        'error':"Problem with parameter",
        "description":"Problem with value for parameter {}".format(exc.detail),
        "error_code":"4004"},status_code=500)


@app.exception_handler(4005)
async def no_q(request, exc):
    return JSONResponse({
        'error':"No paramter _q_",
        "description":"The parameter _q_ with terms to search was not provided",
        "error_code":"4005"},status_code=500)


@app.exception_handler(4006)
async def no_terms(request, exc):
    return JSONResponse({
        'error':"No terms in parameter _q_",
        "description":"The parameter _q_ does not contain at least one known term to be search for. Its value is: {}".format(exc.detail),
        "error_code":"4006"},status_code=500)


## Default values for parameters
g_min_sim=0.4 # Minimum similitud to consider a term related to a seed term
g_min_sim_adaptive=0.60 # Minimum similitud to add a term to the following search
g_max_adaptive_terms=20 # Maximum number of nodes to be added in the following search
g_step=1 # The number of years to skip
g_win_size_prev=0 # Number of previous years to aggregate
g_win_size_next=2 # Number of next years to aggregate
g_max_links=400 # Maximum number of links in the network
g_only_seed="off" # Only display seed terms
g_adaptive="off" # Allow addaptative mode, this is to add new terms as time passes
g_word_boost=1.5 # Value to add to seed terms instead of starting with zero
g_top_related=100 # Number of terms related to the seed
g_smoothing="linear" # Smoothing to use for aggregations "linear" or "gauss"
g_c_gauss=2.0 # Value for gauss aggragation
g_num_words=1000 # Number of embedddings to return
g_version="1.0" # Version of the API


def param_arg(request,key,def_val):
    ''' Check if a _key_ parameter is present in the query of a url (_request_), otherwise it returns the _def_val_ value '''
    try:
        return request.query_params[key]
    except KeyError:
        return def_val

def get_params(request):
    ''' Gets the standard parameters for the api '''
    pars={}
    try:
        pars['min_sim'] = float(param_arg(request,'min_sim',g_min_sim))
    except ValueError:
        raise HTTPException(4004, detail="min_sim")
    try:
        pars['step'] = int(param_arg(request,'step',g_step))
    except ValueError:
        raise HTTPException(4004, detail="step")
    try:
        pars['win_size_prev'] = int(param_arg(request,'win_size_prev',g_win_size_prev))
    except ValueError:
        raise HTTPException(4004, detail="win_size_prev")
    try:
        pars['win_size_next'] = int(param_arg(request,'win_size_next',g_win_size_next))
    except ValueError:
        raise HTTPException(4004, detail="win_size_next")
    try:
        pars['max_links'] = int(param_arg(request,'max_links',g_max_links))
    except ValueError:
        raise HTTPException(4004, detail="max_links")
    try:
        pars['top_related'] = int(param_arg(request,'top_related',g_top_related))
    except ValueError:
        raise HTTPException(4004, detail="top_related")
    pars['adaptive'] = bool(param_arg(request,'adaptive',g_adaptive)=='on')
    pars['only_seed'] = bool(param_arg(request,'only_seed',g_only_seed)=="on")
    pars['smoothing'] = param_arg(request,'smoothing',g_smoothing)
    try:
        pars['word_boost'] = float(param_arg(request,'word_boost',1.5))
    except ValueError:
        raise HTTPException(4004, detail="word_boost")
    try:
        pars['c_gauss'] = float(param_arg(request,'c_gauss',g_c_gauss))
    except ValueError:
        raise HTTPException(4004, detail="c_gauss")
    try:
        pars['min_sim_adaptive'] = float(param_arg(request,'min_sim_adaptive',g_min_sim_adaptive))
    except ValueError:
        raise HTTPException(4004, detail="min_sim_adaptive")
    try:
        pars['max_adaptive_terms'] = int(param_arg(request,'max_adaptive_terms',g_max_adaptive_terms))
    except ValueError:
        raise HTTPException(4004, detail="max_adaptive_terms")
    return pars


@app.route('/')
async def index(request):
    """ Status and version """
    if len(models_)==0:
        return JSONResponse({
            'name':'oceanicex_api',
            'status': 'error: no_models',
            'version':g_version})
    else:
        return JSONResponse({
            'name':'oceanicex_api',
            'status': 'ok',
            'version':g_version})
app.add_route('/v1/',index)

@app.route('/years')
async def years(request):
    """
    Returns available years for analysis
    """

    if len(models_)>0:
        years=[y for y in models_.keys()]
        return JSONResponse({'years':years, 'total':len(years) })
    else:
        raise HTTPException(4001, detail='')
app.add_route('/v1/years',years)

@app.route('/dim')
async def dim(request):
    """ Returns dimensionality of vectors"""

    if len(models_)>0:
        for y in models_.keys():
            break
        return JSONResponse({'embedding_dimension': models_[y].dim })
    else:
        raise HTTPException(4001, detail='')
app.add_route('/v1/dim',dim)

@app.route('/voca/{year}')
async def voca(request):
    """ Returns vocabulary for the year"""
    try:
        year = int(request.path_params['year'])
    except ValueError:
        raise HTTPException(4002, detail='year')
    if year in models_:
        voca = [v for v,_ in models_[year]]
        return JSONResponse({'vocabulary': voca, 'total': len(voca) })
    else:
        raise HTTPException(4003, detail=year)
app.add_route('/v1/voca/{year}',voca)

@app.route('/random_embeddings/{year}')
async def random_embeddings(request):
    """ Random embeddings for a year """
    if len(models_)==0:
        raise HTTPException(4001, detail='')
    try:
        year = int(request.path_params['year'])
    except ValueError:
        raise HTTPException(4002, detail='year')
    if not year in models_:
        raise HTTPException(4003, detail=year)
    try:
        num_words = int(param_arg(request,'num_words',g_num_words))
    except ValueError:
        raise HTTPException(4004, detail="num_words")
    idx  = random.sample(range(len(models_[year])),num_words)
    voca = [v for v,_ in models_[year][idx]]
    vecs = [v.tolist() for _,v in models_[year][idx]]
    return JSONResponse(
            {'terms': voca,
             'embeddings': vecs,
             'total': num_words})
app.add_route('/v1/random_embeddings/{year}',random_embeddings)

@app.route('/embeddings/{year}')
async def embeddings(request):
    """ embeddings for a year from the parameter _q_"""
    if len(models_)==0:
        raise HTTPException(4001, detail='')
    try:
        year = int(request.path_params['year'])
    except ValueError:
        raise HTTPException(4002, detail='year')
    if not year in models_:
        raise HTTPException(4003, detail=year)
    try:
        terms = [t for t in [ t.strip().lower() for t in request.query_params['q'].split(' ')] if len(t) > 0 and t in models_[year]]
    except KeyError:
        raise HTTPException(4005, detail='q')
    if len(terms)==0:
        raise HTTPException(4006, detail=request.query_params['q'])

    vecs = [models_[year].query(t).tolist() for t in terms if t in models_[year]] 
    missing = [t for t in [ t.strip().lower() for t in request.query_params['q'].split(' ')] if len(t) > 0 and not t in models_[year]]
    return JSONResponse(
            {'terms': terms,
             'not_used_terms': missing,
             'embeddings': vecs,
             'total': len(terms)})
app.add_route('/v1/embeddings/{year}',embeddings)


@app.route('/networks/{ini_year}/{fin_year}')
async def networks(request):
    """ return network for terms from ini_year to fin_year """
    if len(models_)==0:
        raise HTTPException(4001, detail='-')
    try:
        ini_year = int(request.path_params['ini_year'])
    except ValueError:
        raise HTTPException(4002, detail='ini_year')
    if not ini_year in models_:
        raise HTTPException(4003, detail=ini_year)
    try:
        fin_year = int(request.path_params['fin_year'])
    except ValueError:
        raise HTTPException(4002, detail='fin_year')
    if not fin_year in models_:
        raise HTTPException(4003, detail=fin_year)
    try:
        terms = [t for t in [ t.strip().lower() for t in request.query_params['q'].split(' ')] if len(t) > 0]
    except KeyError:
        raise HTTPException(4005, detail='q')
    if len(terms)==0:
        raise HTTPException(4006, detail=request.query_params['q'])
    params=get_params(request)
    step=params['step']
    networks=[]
    new_terms=set()
    for year in range(ini_year,fin_year+1,step):
        nodes, edges, new_terms = extract_network(models_[year],terms,new_terms,
                top_related=params['top_related'],
                min_sim=params['min_sim'],
                min_sim_adaptive=params['min_sim_adaptive'],
                max_adaptive_terms=params['max_adaptive_terms'],
                adaptive=params['adaptive'])
        networks.append({
                'nodes':nodes,
                'edges':edges,
                'terms':terms,
                'title':str(year),
                'total_nodes':len(nodes),
                'total_edges':len(edges),
                'total_terms':len(terms)
            })
        if not params['adaptive']:
            new_terms=set()

    return JSONResponse(
            {'networks': networks,
             'total_networks': len(networks),
             })
app.add_route('/v1/networks/{ini_year}/{fin_year}',networks)

@app.route('/networks_aggregate/{ini_year}/{fin_year}')
async def networks_agg(request):
    """ return network aggregates of networks for terms from ini_year to fin_year """
    if len(models_)==0:
        raise HTTPException(4001, detail='-')
    try:
        ini_year = int(request.path_params['ini_year'])
    except ValueError:
        raise HTTPException(4002, detail='ini_year')
    if not ini_year in models_:
        raise HTTPException(4003, detail=ini_year)
    try:
        fin_year = int(request.path_params['fin_year'])
    except ValueError:
        raise HTTPException(4002, detail='fin_year')
    if not fin_year in models_:
        raise HTTPException(4003, detail=fin_year)
    try:
        terms = [t for t in [ t.strip().lower() for t in request.query_params['q'].split(' ')] if len(t) > 0]
    except KeyError:
        raise HTTPException(4005, detail='q')
    if len(terms)==0:
        raise HTTPException(4006, detail=request.query_params['q'])
    params=get_params(request)
    step=params['step']
    networks=[]
    info=[]
    new_terms=set()
    for year in range(ini_year,fin_year+1,step):
        nodes, links, new_terms = extract_network(models_[year],terms,new_terms,
                top_related=params['top_related'],
                min_sim=params['min_sim'],
                min_sim_adaptive=params['min_sim_adaptive'],
                max_adaptive_terms=params['max_adaptive_terms'],
                adaptive=params['adaptive'])
        info.append((nodes,links))
        if not params['adaptive']:
            new_terms=set()
    networks=aggregate_networks(info,terms,new_terms,ini_year,fin_year,
            step=params['step'],
            win_size_prev=params['win_size_prev'],
            win_size_next=params['win_size_next'],
            c_gauss=params['c_gauss'],
            smoothing=params['smoothing'],
            max_links=params['max_links'])

    return JSONResponse(
            {'networks': networks,
             'total_networks': len(networks),
             })
app.add_route('/networks_aggregate/{ini_year}/{fin_year}',networks_agg)

@app.route('/stream/{ini_year}/{fin_year}')
async def stream(request):
    """ return stream information for terms from ini_year to fin_year """
    if len(models_)==0:
        raise HTTPException(4001, detail='-')
    try:
        ini_year = int(request.path_params['ini_year'])
    except ValueError:
        raise HTTPException(4002, detail='ini_year')
    if not ini_year in models_:
        raise HTTPException(4003, detail=ini_year)
    try:
        fin_year = int(request.path_params['fin_year'])
    except ValueError:
        raise HTTPException(4002, detail='fin_year')
    if not fin_year in models_:
        raise HTTPException(4003, detail=fin_year)
    try:
        terms = [t for t in [ t.strip().lower() for t in request.query_params['q'].split(' ')] if len(t) > 0]
    except KeyError:
        raise HTTPException(4005, detail='q')
    if len(terms)==0:
        raise HTTPException(4006, detail=request.query_params['q'])

    params=get_params(request)
    step=params['step']
    networks=[]
    info=[]
    new_terms=set()
    all_terms=set(terms)
    for year in range(ini_year,fin_year+1,step):
        nodes, links, new_terms = extract_network(models_[year],terms,new_terms,
                top_related=params['top_related'],
                min_sim=params['min_sim'],
                min_sim_adaptive=params['min_sim_adaptive'],
                max_adaptive_terms=params['max_adaptive_terms'],
                adaptive=params['adaptive'])
        info.append((nodes,links))
        all_terms.update(new_terms)
        if not params['adaptive']:
            new_terms=set()
    networks=aggregate_networks(info,terms,new_terms,ini_year,fin_year,
            step=params['step'],
            win_size_prev=params['win_size_prev'],
            win_size_next=params['win_size_next'],
            c_gauss=params['c_gauss'],
            smoothing=params['smoothing'],
            max_links=params['max_links'])

    labels,stream=get_stream(networks,list(all_terms),ini_year,fin_year,
            step=params['step'],
            only_seed=params['only_seed'],
            word_boost=params['word_boost'])

    return JSONResponse(
            {'years': labels,
             'stream': stream,
             'total_terms': len(stream),
             'total_years': len(labels),
             })
app.add_route('/v1/stream/{ini_year}/{fin_year}',networks_agg)

@app.route('/network/{year}')
async def network(request):
    """ Network for a term in specified year """
    if len(models_)==0:
        raise HTTPException(4001, detail='')
    try:
        year = int(request.path_params['year'])
    except ValueError:
        raise HTTPException(4002, detail='year')
    if not year in models_:
        raise HTTPException(4003, detail=year)
    try:
        terms = [t for t in [ t.strip().lower() for t in request.query_params['q'].split(' ')] if len(t) > 0 and t in models_[year]]
    except KeyError:
        raise HTTPException(4005, detail='q')
    if len(terms)==0:
        raise HTTPException(4006, detail=request.query_params['q'])
  
    params=get_params(request)
    nodes, edges, new_terms = extract_network(models_[year],terms,set(),
            top_related=params['top_related'],
            min_sim=params['min_sim'],
            min_sim_adaptive=params['min_sim_adaptive'],
            max_adaptive_terms=params['max_adaptive_terms'],
            adaptive=params['adaptive'])

    return JSONResponse(
            {'terms': terms,
             'nodes': nodes,
             'edges': edges,
             'title': str(year),
             'total_terms': len(terms),
             'total_nodes': len(nodes),
             'total_edges': len(edges),
             })
app.add_route('/v1/network/{year}',network)

if __name__ == '__main__':
    uvicorn.run(app, host=api_url, port=api_port,reload=True,log_level="info",proxy_headers=True)
